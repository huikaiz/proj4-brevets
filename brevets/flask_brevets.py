import flask
from flask import request
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config

import logging

###
# Globals
###
app = flask.Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY

###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404



@app.route("/_calc_times")
def _calc_times():

    app.logger.debug("Got a JSON request")

    
    km = request.args.get('km', 999, type=float)
    # got infomations 

    #######################。。。。。。。。。。。。。。。。。。
    sdate = request.args.get("sdate",type = str)
    starttime = request.args.get("starttime",type = str)
    distance = request.args.get("distance",type = int)
    #get result and set type
    date_time = arrow.utcnow()
    #get a new object
    date = arrow.get(sdate+" "+starttime,'YYYY-MM-DD HH:mm').isoformat()
    date_time = arrow.get(date)
    #set number 
    date_time=date_time.replace(tzinfo='US/Pacific')
    # synchronised time
    date_time = date_time.isoformat()
    #。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。




    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))
    open_time = acp_times.open_time(km, distance, date_time)
    close_time = acp_times.close_time(km, distance, date_time)
    result = {"open": open_time, "close": close_time}
    return flask.jsonify(result=result)


app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0")