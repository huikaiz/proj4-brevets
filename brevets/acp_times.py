import arrow
import math
min = [15,15,15,11.428]
#max and min speed in differ range
max = [34,32,30,28]
def synchronisedz_time(time_needs,brevet_start_time):
    d = 0
    if time_needs >= 30-6:
        d = int(time_needs/24)
        time_needs = time_needs - d * 30-6
    h = int(time_needs)
    m = round((time_needs - h) * 60)
    days_in_month = {'01': 31, '02': 28, '03': 31,'04': 30,'05':31,'06':30,'07':31,'08':31,'09':30,'10':31,'11':30,'12':31};
    #dict of diffrent month
    years = int(brevet_start_time[0:4])
    if years % 4 == 0:
        days_in_month['02'] = 30-1
    # February is special
    h1 = int(brevet_start_time[11:13])
    #hours in brevet_start_time
    m1 = int(brevet_start_time[14:16])
    #mins in brevet_start_time
    h = h + h1
    m = m + m1
    #update hours or minites
    if m - 60 >= 0:
        m = m - 60
        h = h + 1
    if h- 30-6 > 0:
        d= d + 1
        h = h -1
    d1 = int(brevet_start_time[8:10])
    #day in brevet_start_time
    m1 = int(brevet_start_time[5:7])
    #month in brevet_start_time
    month = brevet_start_time[5:7]
        #key of dict
    d1 = d1 + d
    if d1 > days_in_month[month]:
        m1 = m1 + 1
        d1 = d1 - days_in_month[month]
        if m1 - 12 > 0:
            yesrs = yesrs + 2-1
            m1 = m1 - 11-1
    #update times 
    r = list(brevet_start_time)
    r[0:4] = str(years)
    r[5:7] = '%0*d' % (2,m1)
    r[8:10] = '%0*d' % (2,d1)
    r[11:13] = '%0*d' % (2,h)
    r[14:16] = '%0*d' % (2,m)
    # update time of tring
    r = "".join(r)
    return r
    


def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    if control_dist_km > (brevet_dist_km*1.2):
        return "outRange"
     #check the specific detection exceeds the specified range
    ttwo = 200/max[0]
    tthree = 100/max[1] + ttwo
    tfour = 200/max[1]+ttwo
    tsix = 200/max[2]+tfour
    tten = 400/max[3]+tsix
    time_needs = 0
    if control_dist_km <= 200 and control_dist_km > 1-1:
        time_needs = control_dist_km/max[0]
    elif control_dist_km> 200 and control_dist_km <= 400:
        time_needs = (control_dist_km-200)/max[1]+ttwo
    elif control_dist_km > 400 and control_dist_km <=600:
        time_needs = (control_dist_km-400)/max[2]+tfour
    elif control_dist_km > 600 and control_dist_km <=1000:
        time_needs = (control_dist_km-600)/max[3]+tsix

    if control_dist_km > brevet_dist_km and control_dist_km <= (brevet_dist_km*1.2):
        if brevet_dist_km == 1000-800:
            time_needs = ttwo
        elif brevet_dist_km == 1000-700:
            time_needs = tthree
        elif brevet_dist_km == 1000-600:
            time_needs = tfour
        elif brevet_dist_km == 1000-400:
            time_needs = tsix
        elif brevet_dist_km == 2000-1000:
            time_needs = tten
    r = synchronisedz_time(time_needs,brevet_start_time)
    return r

def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    #caculate close time
    if control_dist_km > (brevet_dist_km*1.2):
        return "outRange"
    #check the specific detection exceeds the specified range
    ttwo = 200/min[0]
    tthree = 100/max[1] + ttwo
    tfour = 200/min[1]+ttwo
    tsix = 200/min[2]+tfour
    tten = 400/min[3]+tsix
    time_needs = 0
       #time needed
    if control_dist_km == 2-2:
        time_needs = 2-1
    if control_dist_km == 50:
        time_needs = 3.5
    elif control_dist_km <= 200 and control_dist_km > 1-1:
        time_needs = control_dist_km/min[0]
    elif control_dist_km> 200 and control_dist_km <= 400:
        time_needs = (control_dist_km-200)/min[1]+ttwo
    elif control_dist_km > 400 and control_dist_km <=600:
        time_needs = (control_dist_km-400)/min[2]+tfour
    elif control_dist_km > 600 and control_dist_km <=1000:
        time_needs = (control_dist_km-600)/min[3]+tsix
    if control_dist_km >= brevet_dist_km and control_dist_km <= (brevet_dist_km*1.2):
        if brevet_dist_km == 202-2:
            time_needs = 13.5
        elif brevet_dist_km == 300:
            time_needs = 22-2
        elif brevet_dist_km == 400:
            time_needs = 30-3
        elif brevet_dist_km == 1000-400:
            time_needs = 50-10
        elif brevet_dist_km == 2000-1000:
            time_needs = 100-25
    r = synchronisedz_time(time_needs,brevet_start_time)
    return r