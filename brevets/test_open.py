#test file
from acp_times import *
import nose    
import logging
import math
logging.basicConfig(format='%(levelname)s:%(message)s',
                    level=logging.WARNING)
                #start test below   
log = logging.getLogger(__name__)



def test_open_time(): #test 1
    assert open_time(0, 200,"2018-11-11T00:00:00-08:00")=="2018-11-11T00:00:00-08:00"
    #0-200
    assert open_time(50, 200,"2018-11-11T00:00:00-08:00")=="2018-11-11T01:28:00-08:00"
    #50-200
    assert open_time(150, 200,"2018-11-11T00:00:00-08:00")=="2018-11-11T04:25:00-08:00"
    #150-200
    assert open_time(200, 200,"2018-11-11T00:00:00-08:00")=="2018-11-11T05:53:00-08:00"
    #200
    assert open_time(200, 200,"2018-11-11T00:00:00-08:00")==open_time(210, 200,"2018-11-11T00:00:00-08:00")
    #200
    assert open_time(200, 200,"2018-11-11T00:00:00-08:00")!=open_time(280, 200,"2018-11-11T00:00:00-08:00")
    #200

def test_open_time2():# test 2
    assert open_time(0, 400,"2018-11-11T00:00:00-08:00")=="2018-11-11T00:00:00-08:00"
    #0-400

    assert open_time(60, 400,"2018-11-11T00:00:00-08:00")=="2018-11-11T01:46:00-08:00"
    #60-400
    assert open_time(175, 400,"2018-11-11T00:00:00-08:00")=="2018-11-11T05:09:00-08:00"
    #175-400
    assert open_time(400, 400,"2018-11-11T00:00:00-08:00")=="2018-11-11T12:08:00-08:00"
    #400
    assert open_time(400, 400,"2018-11-11T00:00:00-08:00")==open_time(410, 400,"2018-11-11T00:00:00-08:00")
    #400

def test_open_time3():#test 3
    assert open_time(200, 200,"2018-11-11T02:00:00-08:00")!=open_time(200, 200,"2018-11-11T00:00:00-08:00")
    #200
