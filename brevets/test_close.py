#test file
from acp_times import *
import nose    
import logging
import math
logging.basicConfig(format='%(levelname)s:%(message)s',
                    level=logging.WARNING)
#start test below                  
log = logging.getLogger(__name__)



def test_close_time():#test 1
    assert close_time(0, 200,"2018-11-11T00:00:00-08:00")=="2018-11-11T01:00:00-08:00"
    #0-200
    assert close_time(50, 200,"2018-11-11T00:00:00-08:00")=="2018-11-11T03:30:00-08:00"
    #50-200
    assert close_time(150, 200,"2018-11-11T00:00:00-08:00")=="2018-11-11T10:00:00-08:00"
    #150-200
    assert close_time(200, 200,"2018-11-11T00:00:00-08:00")=="2018-11-11T13:30:00-08:00"
    #200

def test_close_time2():#test2
    assert close_time(200, 200,"2018-11-11T00:00:00-08:00")==close_time(210, 200,"2018-11-11T00:00:00-08:00")
    #200 
    assert close_time(200, 200,"2018-11-11T00:00:00-08:00")!=close_time(280, 200,"2018-11-11T00:00:00-08:00")
    #200